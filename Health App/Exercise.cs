﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Media.Imaging;

namespace Health_App
{

    /********************************************************
     * Project: Health App
     * Author: Mobin Akhter
     * Date: April 20, 2020
     * Class Name: Exercise.cs
     * Class Description: This class is responsible for 
     * implementing all the methods that are required in
     * the Exercise UI so that it provides all the 
     * functionality to the user. 
     ********************************************************/

    class Exercise
    {
        // Declare field variables here
        /// <summary>
        /// This field variable will be responsible for all the "normal" exercises, meaning that 
        /// gifs of without weight exercises will be added to this field variable
        /// </summary>
        private static List<BitmapImage> _exercises;

        /// <summary>
        /// This field variable will be responsible for all the exercises that ensure
        /// the user to use weights.
        /// </summary>
        private static List<BitmapImage> _equipmentExercises;

        /// <summary>
        /// This field variable is respnsible for storing all the exercise for old people, so there 
        /// will be mostly exercises without weights.
        /// </summary>
        private static List<BitmapImage> _oldPeopleExercises;

        /// <summary>
        /// Adding this field variable so that we can have the separation of concerns.
        /// And adding these gifs in another field variable would make it more difficult to 
        /// understand.
        /// </summary>
        private static List<BitmapImage> _uiLaunchGifs;

        // Create a user object
        //User obj = new User();

        // Constructor below
        public Exercise()
        {
            // Initialization of field variables
            _exercises = new List<BitmapImage>();
            _equipmentExercises = new List<BitmapImage>();
            _oldPeopleExercises = new List<BitmapImage>();
            _uiLaunchGifs = new List<BitmapImage>();
        }
        // Properties below
        // All of them are read/write properties because all of these are being "accessed"/obtained and they are also being "modified".
        public List<BitmapImage> Exercises
        {
            get { return _exercises; }
            set { _exercises = value; }
        }
        public List<BitmapImage> EquipmentExercise
        {
            get { return _equipmentExercises; }
            set { _equipmentExercises = value; }
        }
        public List<BitmapImage> OldPeopleExercise
        {
            get { return _oldPeopleExercises; }
            set { _oldPeopleExercises = value; }
        }
        public List<BitmapImage> UiLaunchGifs
        {
            get { return _uiLaunchGifs;}
            set { _uiLaunchGifs = value;}
        }
        /// <summary>
        /// The purpose of this method is to provide a user with an option that is best for them, and therefore it is 
        /// recommended to do this exercise. At this point, I think this method will be called when our user has specific
        /// details about themselves.
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="age"></param>
        /// <param name="weightGoal"></param>
        public List<BitmapImage> RecommendedExercise()
        {
            // TODO: Provide the user with specific exercises if they want to lose and gain weight.
            if(User.WeightToGain<6)
            {
                _exercises = new List<BitmapImage>();
                _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/benchPress.gif")));
                _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise.gif")));
                _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/plank.gif")));
                _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise7.gif")));
            }

            else if(User.WeightToGain>5 && User.WeightToGain < 10)
            {
                _exercises = new List<BitmapImage>();

            }
            return _exercises;

            // TODO: Add more scenarios.

            // TODO: And make sure to provide easier exercise if age > something



        }
        /// <summary>
        /// If the user is not in the mood for an exercise that will be helpful to them in gaining/losing weight, they have
        /// the option to do an easy exercise, because some exercise is better than no exercise. Say, level 2
        /// Note: This is a general "easy" exercise, and not for our specific user, same goes for the two methods below
        /// </summary>
        /// <param name="gender"></param>
        public List<BitmapImage> EasyExercise()
        {
            
            _exercises = new List<BitmapImage>(); // Initialized the field variable
            // In this method, and the methods below, "Add" the gifs to the list that it most appropriate to.

            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EasyExercise/easyExercise.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EasyExercise/easyExercise2.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EasyExercise/easyExercise3xtimes.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EasyExercise/tenSecondEasyExercise.gif")));

            return _exercises;
        }
        /// <summary>
        /// This is an exercise that is not easy, and not difficult. It will be an exercise that the majority of the people
        /// can perform.
        /// Level 5
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        public List<BitmapImage> MediumDifficultyExercise()
        {
            _exercises = new List<BitmapImage>();
            // TODO: Implement this just like the easy exercise method.
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/MediumLevelExercise/mediumDifficultExercise.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/MediumLevelExercise/mediumLevelExercise2.gif")));

            return _exercises;
        }
        /// <summary>
        /// This method is responsible for providing the user with an exercise that around 7/10 people would find difficult 
        /// to complete.
        /// Level 8
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        public List<BitmapImage> DifficultExercise()
        {
            _exercises = new List<BitmapImage>();
            // TODO: Implement this just like the tough exercise method.
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/DifficultExercise/difficultExercise.gif")));
            return _exercises;
        }
        /// <summary>
        /// This method is responsible for providing the user with an exercise that almost everyone would find challenging
        /// to complete.
        /// Level 9.5 worthy exercise; 10 is the highest.
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        public List<BitmapImage> ToughExercise()
        {
            _exercises = new List<BitmapImage>();
            // TODO: Make this more efficient, infact make everything more efficient; as efficient as it can be.
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise2.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise3.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise4.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise5.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise6.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise7.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise8.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ToughExercise/toughExercise9.gif")));

            return _exercises; 
        }
        /// <summary>
        /// This will be a fairly easy exercise for our old (age>65) user. It will not consist of equipments, it will simply be exercises
        /// that focuses on most parts of the body to keep their muscles active.
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        public List<BitmapImage> ExerciseForOldPeople()
        {
            _oldPeopleExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ElderlyExercise/elderlyExercise.gif")));
            _oldPeopleExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ElderlyExercise/ExerciseForOldPeople.gif")));
            _oldPeopleExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ElderlyExercise/oldExercise.gif")));
            _oldPeopleExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ElderlyExercise/oldPeople.gif")));
            _oldPeopleExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/ElderlyExercise/oldPeopleExercise.gif")));

            // return this list
            return _oldPeopleExercises;
            
        }
        // Implementing a method that contains no equipment exercise.
        public List<BitmapImage> NoEquipmentExercise()
        {
            _exercises = new List<BitmapImage>(); // Initialized it

            // Fill the list with exercises that do not involve equipment.
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/noEquipment4.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/noEquipment5.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/noEquipment6.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/noEquipment7.gif")));
            _exercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/NoEquipments/plank.gif")));

            return _exercises;
        }
        // Now, declare a method that ONLY has exercises with equipment.
        public List<BitmapImage> EquipmentExercises()
        {
            _equipmentExercises = new List<BitmapImage>();

            // Fill the list with exercises that involve equipments.
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/benchPress.gif")));
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/manDumbell.gif")));
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/manDumbells.gif")));
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/manDumbellsGaining.gif")));
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/manGainingWeight.gif")));
            _equipmentExercises.Add(new BitmapImage(new Uri("ms-appx:///Assets/EquipmentExercise/manGainingWeightDumb.gif")));
            // Code above does not seem to be efficient but making it work at the moment

            // Return this list since the type of the method is List<BitmapImage>
            return _equipmentExercises;
        }
        /// <summary>
        /// This method will add a few gifs, that shall be displayed to the user when they come to the Workout UI.
        /// This is because it does not look good when there is not anything in the middle of the screen, which 
        /// would seem odd to the user.
        /// </summary>
        /// <returns></returns>
        public List<BitmapImage> StartUpScreenGif()
        {
            _uiLaunchGifs.Add(new BitmapImage(new Uri("ms-appx:///Assets/MiscellaneousGifs/AreYouReady.gif")));
            _uiLaunchGifs.Add(new BitmapImage(new Uri("ms-appx:///Assets/MiscellaneousGifs/AreYouReadyy.gif")));
            _uiLaunchGifs.Add(new BitmapImage(new Uri("ms-appx:///Assets/MiscellaneousGifs/readyForThis.gif")));

            return _uiLaunchGifs;
        }
    }// end of class Exercise
}// end of namespace

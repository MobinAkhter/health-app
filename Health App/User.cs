﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Health_App
{
    //create the gender enum
    //TEST 
    //TEST
    //uh yeah
    //shark cathes an airplane
    public enum GenderType
    {
        Male = 1,
        Female = 2,
    }
    //declare the vegan type 
    public enum SoloPlantEater
    {
        Vegan = 1,
        Vegetarian = 2,
        Neither = 3,
    }
    class User //Author: Anais Zulfequar
    {
        //implement the gender field variable
        private static GenderType _gender;
        //implement the weight field variable
        private static float _weight;
        //add the weight goal feild variable
        private static float _weightGoal;
        ////add the name field variable
        //private string _name;
        //add the height field variable
        private float _height;
        //add the age field variable
        private int _age;
        //implement the diet field variable
        private Diet _diet;
        private Time _time;
        private Exercise _exercise;
        private Schedule _schedule;
        private WeightClass _weightClass;
        //create the planteEater field variable
        private static SoloPlantEater _plantEater;
        //create the User constructor

       
        public User(GenderType gender, int age, float weight,  float height, float weightGoal, SoloPlantEater plantEater) 
        {
            _gender = gender;
            _weightGoal = weightGoal;
            _weight = weight;
            _height = height;
            _age = age;
            _plantEater = plantEater;
            _diet = new Diet();
            _exercise = new Exercise();
            _time = new Time();

            //_weightClass = new WeightClass();
        }

        public User()
        {

        }

        //public void Diet(float weight, float weightGoal, SoloPlantEater plantEater)
        //{
        //    //create diet object
        //    _diet = new Diet(weight, weightGoal, plantEater);
        //}

        //implement the read only properties for each field variable
        //public string Gender
        //{
        //    get { return _gender; }
        //}

        public int Age
        {
            get { return _age; }
        }


        public static float WeightGoal
        {
            get { return _weightGoal; }
            set { _weightGoal = value; }
        }

        //public string Name
        //{
        //    get { return _name; }
        //}
        //implement the read-only properties
        public static float Weight
        {
            get { return _weight; }
        }

        public float Height
        {
            get { return _height; }
        }

        //weight to lose property to calculate how much weight the user needs to lose from his original weight
        public static float WeightToLose
        {
            get { return _weight - _weightGoal; }
        }
        //weight to gain property to calculate how much weight the user needs to gain from his original weight
        public static float WeightToGain
        {
            get { return _weightGoal - _weight; }
        }

        public static SoloPlantEater PlantEater
        {
            get { return _plantEater; }
        }
        public static GenderType Gender
        {
            get { return _gender; }
        }


    }
}

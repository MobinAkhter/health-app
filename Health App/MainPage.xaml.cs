﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Health_App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page 
    {
       
        
        private User _user;
        private WeightClass _weightClass;
        public MainPage()
        {
            this.InitializeComponent();
            

        }
        // Also, I feel like this button is not necessary anymore, probably let the user exit the app through this button
        // Though we need this information when the user clicks the other 2 buttons. 
        
        private void FinishButton(object sender, RoutedEventArgs e)
        {
            //this user input will go to the user class first
            
            //get all that input from ui and parse them
            int age = int.Parse(_txtAge.Text);
            int height = int.Parse(_txtHeight.Text);
            float weight = int.Parse(_txtWeight.Text);
            float weightGoal = int.Parse(_txtAmount.Text);
            GenderType gender = (GenderType)_genderBox.SelectedIndex; //Apparently app can recognise only male and vegan
            SoloPlantEater plantEater = (SoloPlantEater)_plantEater.SelectedIndex;
            _user = new User(gender, age, weight, height, weightGoal, plantEater);
            //Tell the user their BMI
            _weightClass = new WeightClass(0);
            _weightClass.DetermineWeightClass();

           

            CreateTextFile();

        }
        private void _viewDiet_Click(object sender, RoutedEventArgs e) // When the user clicks "Go to diet button"
        {
            this.Frame.Navigate(typeof(MainPage1));
        }

        private void _viewExercise_Click(object sender, RoutedEventArgs e) // When the user clicks "Go to exercise button"
        {
            this.Frame.Navigate(typeof(MainPage2));
        }

        // Trying to see if I can store user input into a .Json text file.
        // At least the initial input. Documentation shall be my savior once again :)
        // Copying stuff from documentation but using logic. Hopefully it works on the first try.
        // Since we store the information in the "Finish" button about the user, all this should be called in that event handler. 
        // Create sample file; replace if exists.
        public async void CreateTextFile()
        {
            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile =
                await storageFolder.CreateFileAsync("DataPersistence.txt",
                    Windows.Storage.CreationCollisionOption.ReplaceExisting);
            WriteTextFile();
            ReadTextFile();
        }
        public async void WriteTextFile()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync("DataPersistence.txt");
            await Windows.Storage.FileIO.WriteTextAsync(sampleFile, "Swift as a shadow");
        }
        public async void ReadTextFile()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync("DataPersistence.txt");
            string text = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
        }
    }
}

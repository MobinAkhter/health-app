﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Health_App
{
    //create enum to see if user is vegan, vegetarian or neither

    
    public class Diet //Author Anais Zulfequar
    {
        //create the messge boxes to name the food
        MessageDialog WeightLossVeganFood1 = new MessageDialog("For breakfast you will have tomato toast. For lunch you will have greek salad. For dinner you will have potato salad");
        MessageDialog WeightLossVeganFood2 = new MessageDialog("For breakfast you will have avocado toast. For lunch you will have greek salad. For dinner you will have a veggie wrap");
        MessageDialog WeightLossVeganFood3 = new MessageDialog("For breakfast you will have blueberry and oaths. For lunch you will have greek salad. For dinner you will have potato salad");

        MessageDialog WeightLossVeggieFood1 = new MessageDialog("For breakfast you will have a green smoothie. For lunch you will have vegetable salad. For dinner you will have a potato salad");
        MessageDialog WeightLossVeggieFood2 = new MessageDialog("For breakfast you will have berry smmothie. For lunch you will have greek salad. For dinner you will have a veggie wrap");
        MessageDialog WeightLossVeggieFood3 = new MessageDialog("For breakfast you will have blueberry and oaths. For lunch you will have greek salad. For dinner you will have a veggie wrap");

        MessageDialog WeightLossOmnivoreFood1 = new MessageDialog("For breakfast you will have a green smoothie. For lunch you will have greek salad. For dinner you will have a potato salad");
        MessageDialog WeightLossOmnivoreFood2 = new MessageDialog("For breakfast you will have berry smmothie. For lunch you will have vegetable salad. For dinner you will have chicken and potatoes");
        MessageDialog WeightLossOmnivoreFood3 = new MessageDialog("For breakfast you will have blueberry and oaths. For lunch you will have blueberry muffins. For dinner you will have a potato and beans");


        MessageDialog WeightGainVeganFood1 = new MessageDialog("For breakfast you will have blueberry and oaths. For lunch you will have a salad. For dinner you will have stir fry pasta");
        MessageDialog WeightGainVeganFood2 = new MessageDialog("For breakfast you will have apples. For lunch you will have a salad. For dinner you will have stir fry pasta");
        MessageDialog WeightGainVeganFood3 = new MessageDialog("For breakfast you will have mangoes. For lunch you will have a salad. For dinner you will have stir fry pasta");

        MessageDialog WeightGainVeggieFood1 = new MessageDialog("For breakfast you will have pancakes. For lunch you will have a cream cheese bagel. For dinner you will have some veggie pizza");
        MessageDialog WeightGainVeggieFood2 = new MessageDialog("For breakfast you will have an omelette. For lunch you will have a salad. For dinner you will have stir fry pasta");
        MessageDialog WeightGainVeggieFood3 = new MessageDialog("For breakfast you will have an omelette. For lunch you will have a salad. For dinner you will have stir fry pasta");

        MessageDialog WeightGainOmnivoreFood1 = new MessageDialog("For breakfast you will have a sausage biscuit. For lunch you will have a burger. For dinner you will have rice with chicken");
        MessageDialog WeightGainOmnivoreFood2 = new MessageDialog("For breakfast you will have a peanut butter jelly sandwich. For lunch you will have a bean burrito. For dinner you will have tuna pasta");
        MessageDialog WeightGainOmnivoreFood3 = new MessageDialog("For breakfast you will have bacon and eggs. For lunch you will have a cream cheese bagel For dinner you will have veggie pizza");


        //create the breakfast, lunch and dinner field variable
        private static List<BitmapImage> _breakfast;
        private static  List<BitmapImage> _lunch;
        private static  List<BitmapImage> _dinner;
       
        //create the constructor
        public Diet()
        {
      
            //create the breackfast, lunch and dinner list
            _breakfast = new List<BitmapImage>();
            _lunch = new List<BitmapImage>();
            _dinner = new List<BitmapImage>();
        }
       
        //create the properties
        public List<BitmapImage> Breakfast
        {
            get { return _breakfast; }
            set { _breakfast = value; }
        }

        public List<BitmapImage> Lunch
        {
            get { return _lunch; }
            set { _lunch = value; }
        }

        public List<BitmapImage> Dinner
        {
            get { return _dinner; }
            set { _dinner = value; }
        }
        //make methods

        public List<BitmapImage> BreakfastList()
        {
            _breakfast = new List<BitmapImage>();
            //add in the bitmap images of the breakfast
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/sandwich.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/omelette.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/pancakes.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/bacon.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/biscuit.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/greensmoothie.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/berrysmoothie.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/blueberryoaths.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/AvocadoToast.jpg")));
            _breakfast.Add(new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/tomatoToast.jpg")));
            return _breakfast;
        }

        //adding in the images for the lunch list
        public List<BitmapImage> LunchList()
        {
            _lunch = new List<BitmapImage>();
            
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/burger.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/burrito.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/bagel.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/vegetableSalad.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/chickenBreast.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/tunaSalad.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg")));
            _lunch.Add(new BitmapImage(new Uri("ms-appx:///Assets/Lunch/muffins.jpg")));
            return _lunch;
        }

        //adding in the images for the dinner list
        public List<BitmapImage> DinnerList()
        {
            _dinner = new List<BitmapImage>();
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/chickenRice.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/pizza.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/tunaPasta.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoSalad.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/veggieBurger.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoBeans.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/veggieWrap.jpg")));
            _dinner.Add(new BitmapImage(new Uri("ms-appx:///Assets/Dinner/chickenPotatoes.jpg")));
            return _dinner;
        }
        /// <summary>
        /// Check method is used to determine which food that the user need depending on the user's weight and weight goal
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        /// <param name="plantEater"></param>
        public void Check(float weight, float weightGoal, SoloPlantEater plantEater)
        {
            if (weight > weightGoal)
            {
                WeightLossFood( weight, weightGoal, plantEater);
                
            }
            else if (weightGoal > weight)
            {
                WeightGainFood(weight, weightGoal, plantEater);
            }
        }
        /// <summary>
        /// Determine which weight loss foods that the user will need depending if he's vegan, vegetarian or neither
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        /// <param name="plantEater"></param>
        public void WeightLossFood(float weight, float weightGoal, SoloPlantEater plantEater)
        {

           if (plantEater == SoloPlantEater.Vegan)
            {
                WeightLossVeganFoodAsync(weight,weightGoal);
            }
           else if(plantEater == SoloPlantEater.Vegetarian)
            {
                WeightLossVeggieFood( weight,  weightGoal);
            }
           else if (plantEater == SoloPlantEater.Neither)
            {
                WeightLossOmnivore(weight, weightGoal);
            }
        }
        /// <summary>
        ///   /// <summary>
        /// Determine which weight gain foods that the user will need depending if he's vegan, vegetarian or neither
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        /// <param name="plantEater"></param>
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        /// <param name="plantEater"></param>
        public void WeightGainFood(float weight, float weightGoal, SoloPlantEater plantEater)
        {


            if (plantEater == SoloPlantEater.Vegan)
            {
                WeightGainVeganFood( weight,weightGoal);
            }
            else if (plantEater == SoloPlantEater.Vegetarian)
            {
                WeightGainVeggieFood( weight, weightGoal);
            }
            else if (plantEater == SoloPlantEater.Neither)
            {
                WeightGainOmnivore(weight, weightGoal);
            }
        }


        /// <summary>
        /// These next methods will choose the food for the user depending the difference between weight and weight goal
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        public async void  WeightLossVeganFoodAsync(float weight, float weightGoal)
        {
     

            if (weight - weightGoal >= 100)
            {

                await WeightLossVeganFood1.ShowAsync();

            }
            else if (weight - weightGoal >= 50 && weight - weightGoal < 100)
            {
                await WeightLossVeganFood2.ShowAsync();

                
            }
            else if (weight - weightGoal < 50)
            {
                await WeightLossVeganFood3.ShowAsync();
            }
        }
        /// <summary>
        /// checs user's weight difference
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        public async void WeightLossVeggieFood(float weight, float weightGoal)
        {
    

            if (weight - weightGoal >= 100)
            {
                await WeightLossVeggieFood1.ShowAsync();
            }
            else if (weight - weightGoal >= 50 && weight - weightGoal < 100)
            {
                await WeightLossVeggieFood2.ShowAsync();
            }
            else if (weight - weightGoal < 50)
            {
                await WeightLossVeggieFood3.ShowAsync();
            }

        }
        /// <summary>
        /// checs user's weight difference
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        public async void WeightLossOmnivore(float weight, float weightGoal)
        {
            if (weight - weightGoal >= 100)
            {
                await WeightLossOmnivoreFood1.ShowAsync();
            }
            else if (weight - weightGoal >= 50 && weight - weightGoal < 100)
            {
                await WeightLossOmnivoreFood2.ShowAsync();
            }
            else if (weight - weightGoal < 50)
            {
                await WeightLossOmnivoreFood3.ShowAsync();
            }
        }
        /// <summary>
        /// checs user's weight difference
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
       public  async void WeightGainVeganFood(float weight, float weightGoal)
        {
    
           
            if (weightGoal - weight >= 100)
            {
                await WeightGainVeganFood1.ShowAsync();
            }
            else if (weightGoal - weight >= 50 && weightGoal - weight < 100)
            {
                await WeightGainVeganFood2.ShowAsync();
            }
            else if (weight - weightGoal < 50)
            {
                await WeightGainVeganFood3.ShowAsync();
            }
        }
        /// <summary>
        /// checs user's weight difference
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        public async void WeightGainVeggieFood(float weight, float weightGoal)
        {
    

            if (weightGoal - weight >= 100)
            {
                await WeightGainVeggieFood1.ShowAsync();
            }
            else if (weightGoal - weight >= 50 && weightGoal - weight < 100)
            {
                await WeightGainVeggieFood2.ShowAsync();
            }
            else if (weight - weightGoal < 50)
            {
                await WeightGainVeggieFood3.ShowAsync();
            }
        }
        /// <summary>
        /// checs user's weight difference
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightGoal"></param>
        public async void WeightGainOmnivore(float weight, float weightGoal)
        {
            if (weightGoal - weight >= 100)
            {
                await WeightGainOmnivoreFood1.ShowAsync();
            }
            else if (weightGoal - weight >= 50 && weightGoal - weight < 100)
            {
                await WeightGainOmnivoreFood2.ShowAsync();
            }
            else if (weight - weightGoal < 50)
            {
                await WeightGainOmnivoreFood3.ShowAsync();
            }
        }


     
    }
}


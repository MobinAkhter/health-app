﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Health_App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage3 : Page
    {
        // ==================================================== For scheudule UI ====================================================
        public MainPage3()
        {
            this.InitializeComponent();
        }

        private void OnDietClick(object sender, RoutedEventArgs e) //Travel to the Diet UI
        {
            this.Frame.Navigate(typeof(MainPage1));
        }

        private void OnWorkoutClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage2));
        }

        private void OnClickSchedule(object sender, RoutedEventArgs e)
        {
            //This will print the number of weeks you will need to repeat the schedule's tasks to complete your goal 

            //if (User.WeightGoal > User.Weight)
            //{

            //    _txtWeeks.Text = Schedule.WeeklyToGain().ToString();
            //}
            //else
            //{

            //    _txtWeeks.Text = Schedule.WeeklyToLose().ToString();
            //}

            //Display the amount of calories the user must eat to meet their goal
            _txtMonCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Monday";
            _txtTuesCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Tuesday";
            _txtWedCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Wednsday";
            _txtThursCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Thrusday";
            _txtFriCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Friday";
            _txtSatCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Saturday";
            _txtSunCalories.Text = $"You must lose {Schedule.CaloriesPerDay.ToString()} Calories on Sunday";

            //Display the foods that the program has chosen 
            _txtMonFood.Text = "Your Monday meals are: PLACEHOLDER";
            _txtTuesFood.Text = "Your Tuesday meals are: PLACEHOLDER";
            _txtWedFood.Text = "Your Wednsday meals are: PLACEHOLDER";
            _txtThursFood.Text = "Your Thursday meals are: PLACEHOLDER";
            _txtFriFood.Text = "Your Friday meals are: PLACEHOLDER";
            _txtSatFood.Text = "Your Saturday meals are: PLACEHOLDER";
            _txtSunFood.Text = "Your Sunday meals are: PLACEHOLDER";


        }
    }
}

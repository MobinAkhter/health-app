﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Health_App
{
    /********************************************************
    * Project: Health App
    * Author: Mobin Akhter
    * Date: April 20, 2020
    * Class Name: Exercise.cs
    * Class Description: This class is responsible for 
    * determining when the app was launched by the user
    * and at what time it was terminated. 
    * Purpose: To basically determine if the user followed 
    * through with our program's instructions or not. 
    *********************************************************/
    class Time
    {
        
        /* Ideas/Concerns:
         * Does it need to be TimeSpan, or should I use DateTime?
         * For the TimeInterval property, do I need to make it a double? Also, do I need a backing field?
         */

        /// <summary>
        /// This field variable is responsible for storing the information about when the user launched the app.
        /// </summary>
        private TimeSpan _appLaunched;
        /// <summary>
        /// This field variable is responsible for storing the information about when the user closed the app.
        /// </summary>
        private TimeSpan _appTerminated;


        // Two argument constructor.
        public Time()
        {
            
        }

        // Properties below
        /// <summary>
        /// This property is responsible for storing the time the user launched the app.
        /// </summary>
        public TimeSpan AppLaunched
        {
            get { return _appLaunched; }

        }
        // Properties below
        /// <summary>
        /// This property is responsible for storing the time when the user terminated the app.
        /// </summary>
        public TimeSpan AppTerminated
        {
            get { return _appTerminated; }
        }
        /// <summary>
        /// This property is responsible for giving access to the other classes about the time 
        /// interval between last the termination and the launching of the app.
        /// </summary>
        public TimeSpan TimeInterval
        {
            get { return _appLaunched - _appTerminated; } // Yep knew it wouldn't work.

        }
        // Methods
        /// <summary>
        /// This method will be used to provide the user with more intense exercises.
        /// Now for this to work, the application needs to know what the previous exercise the user got, or 
        /// it could provide them with an even more intense exercise than the ToughExercise(), though
        /// that is not preferred.
        /// </summary>
        public void MoreIntenseExercise()
        {
            // TODO: Determine what the previous exercise was provided to the user

            // TODO: Provide the user with the option to do a more intense exercise/less intense if they followed up with 
            // the previous exercise.
        }
        /// <summary>
        /// This method will be used to provide an option to the user to choose a less intense exercises.
        /// </summary>
        public void LessIntenseExercise()
        {
            // TODO: Let the user do a less difficult exercise, if they followed through with the instructions.

            // This method's functionality will not be provided to the users who re-open our app after 12 hours 
            // (we assume they did not follow through with the exercise).
            

        }
    }// End of class (Time)
}// End of namespace

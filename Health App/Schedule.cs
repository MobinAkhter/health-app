﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Health_App
{
    /// <summary>
    /// Schedule class deals with the daily schedule that users will interact with. Schedule will keep track of the user's daily exercise schedule and mealtimes for the day
    /// </summary>
    class Schedule
    {
        private static int _weeks;
        private static int _calories;
        private static int _caloriesPerDay;
        public Schedule(int Weeks, int Calories, int CaloriesPerDay)
        {
            _weeks = Weeks;
            _calories = Calories;
            _caloriesPerDay = CaloriesPerDay;
        }

        /// <summary>
        /// This method is designed to determine a reasonable goal for a week of activity and healthy eating IF the user wants to GAIN weight.
        /// If the user wants to gain 20 pounds, theres no way they can do it in a week so therefore we have to split up the goal into multiple weeks.
        /// The max weightgoal is 20 pounds above or below the persons weight right now
        /// This method will divide the weekly workouts and calories to lose 5 or less pounds per week so it is reasonable
        /// </summary>
        /// <returns></returns>
        public static int WeeklyToLose()
        {
            

            int numOfWeeks = 1;
            _weeks = Convert.ToInt32(User.WeightToLose);

            while (_weeks > 5)
            {
                _weeks = Convert.ToInt32(User.WeightToLose) - 5;
                numOfWeeks++;
            }
            return numOfWeeks;

        }

        /// <summary>
        /// This method is designed to determine a reasonable goal for a week of activity and healthy eating IF the user wants to LOSE weight.
        /// If the user wants to lose 20 pounds, theres no way they can do it in a week so therefore we have to split up the goal into multiple weeks.
        /// The max weightgoal is 20 pounds above or below the persons weight right now
        /// This method will divide the weekly workouts and calories to lose 5 or less pounds per week so it is reasonable
        /// </summary>
        /// <returns></returns>
        public static int WeeklyToGain()
        {
            int numOfWeeks = 1;
            _weeks = Convert.ToInt32(User.WeightToGain);
            
            while(_weeks > 5)
            {
                _weeks = Convert.ToInt32(User.WeightToGain) - 5;
                numOfWeeks++;
            }
            return numOfWeeks;
        }

        //Calculate caleries per week depending on how many weeks it will take
        public int WeeklyCalories()
        {
            return 0;

        }

        //properties to calculate how many calories you need to lose to meet ur weight
        public static int Calories
        {
            get {return Convert.ToInt32(User.Weight) * 3500; }
        }
        //
        public static int CaloriesPerDay
        {
            get { return _caloriesPerDay = _calories / 7; }
        }

    }
}

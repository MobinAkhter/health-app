﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace Health_App
{
    class WeightClass
    {
        MessageDialog overWeight = new MessageDialog($" Your BMI is over 27.3, you are overweight");
        MessageDialog underWeight = new MessageDialog($" Your BMI is under 19, you are overweight");
        MessageDialog goodWeight = new MessageDialog($" Your BMI is healthy, you are a healthy weight");
        //declare the BMI field variable
        private float _BMI;

        public WeightClass(float BMI)
        {
            _BMI = BMI;
        }


        public float CalculateBMI()
        {
            //convert height into meters
            float meters = User.Weight * 0.254F;

            //calculate BMI
            _BMI = User.Weight * (meters * meters);

            return _BMI;
        }

        public void DetermineWeightClass()
        {
            //Determine if a male user is overweight, underweight or a good weight
            if (User.Gender == GenderType.Male)
            {
                if (_BMI > 27.8)
                {
                    //User is an overweight male
                    Overweight();
                }
                else if (_BMI < 19)
                {
                    //User is an underweight male
                    Underweight();
                }
                else
                {
                    //User is a good weight
                    HealthyWeight();
                }
            }
            
            //Determine if a female user is overweight, underweight or a good weight
            
            if (User.Gender == GenderType.Female)
            {
                if (_BMI > 27.3)
                {
                    //User is an overweight female
                    Overweight();
                }
                else if (_BMI < 18.5)
                {
                    //User is an underweight female
                    Underweight();
                }
                else
                {
                    //User is a good weight
                    HealthyWeight();
                }

            }

        }

        public async void Overweight()
        {
            await overWeight.ShowAsync();
        }
        public async void Underweight()
        {

            await underWeight.ShowAsync();
            //Give the user foods to help them gain weight/muscle

            //Give the user excersizes to help them gain weight/muscle
        }
        public async void HealthyWeight()
        {
            await goodWeight.ShowAsync();
            //Give the user foods the help them gain muscle

            //Give the user excersises to help them gain muscle
        }
    }

}

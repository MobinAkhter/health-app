﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Health_App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    sealed partial class MainPage1 : Page // Anais Zulfequar
    {
        //  ========================================== FOR DIET UI ================================================== 

        private Diet _diet;
        private BitmapImage _breakfast1;
        private BitmapImage _lunch;
        private BitmapImage _dinner;



        public MainPage1() 
        {
            this.InitializeComponent();

        }
        /// <summary>
        /// Created a property here. Change the name if you want Anais
        /// </summary>
        public Diet Diet
        {
            get { return _diet; }
            set { _diet = value; }
        }

        public BitmapImage Breakfast
        {
            get { return _breakfast1; }
            set { _breakfast1 = value; }
        }

        public BitmapImage Lunch
        {
            get { return _lunch; }
            set { _lunch = value; }
        }
        public BitmapImage Dinner
        {
            get { return _dinner; }
            set { _dinner = value; }
        }

        private void ViewMealPlan(object sender, RoutedEventArgs e)
        {
            Diet _diet = new Diet();

            //call the methods from the diet class
            _diet.BreakfastList();
            _diet.LunchList();
            _diet.DinnerList();
            _diet.Check(User.Weight, User.WeightGoal, User.PlantEater);
            //these methods are used to display the pictures based on user input
            // Similar to the diet class methods but instead of message boxes the output are 3 images  
            
            if (User.WeightGoal > User.Weight)
            {
                WeightGainFoods();
            }
            else if (User.WeightGoal < User.Weight)
            {
                WeightLossFoods();
            }
            //check user's herb type
            void WeightGainFoods()
            {
                if (User.PlantEater == SoloPlantEater.Vegan)
                {
                    WeightGainVeganFoods();
                }
                else if (User.PlantEater == SoloPlantEater.Vegetarian)
                {
                    WeightGainVeggieFoods();
                }
                else if (User.PlantEater == SoloPlantEater.Neither)
                {
                    WeightGainOmnivoreFoods();
                }
            }
            //check user's herb type
            void WeightLossFoods()
            {
                if (User.PlantEater == SoloPlantEater.Vegan)
                {
                    WeightLossVeganFoods();
                }
                else if (User.PlantEater == SoloPlantEater.Vegetarian)
                {
                    WeightLossVeggieFoods();
                }
                else if (User.PlantEater == SoloPlantEater.Neither)
                {
                    WeightLossOmnivoreFoods();
                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightLossVeganFoods()
            {
                if (User.Weight - User.WeightGoal >= 100)
                {

                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/tomatoToast.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoSalad.jpg"));
                    return;
                }
                else if (User.Weight - User.WeightGoal >= 50 && User.Weight - User.WeightGoal < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/AvocadoToast.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                    _dinnerImg.Source =  new BitmapImage(new Uri("ms-appx:///Assets/Dinner/veggieWrap.jpg"));            
                    return;
                }
                else if (User.Weight - User.WeightGoal < 50)
                {
                   _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/blueberryoaths.jpg"));
                   _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                   _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoSalad.jpg"));
           
                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightLossVeggieFoods()
            {
                if (User.Weight - User.WeightGoal >= 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/greensmoothie.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/vegetableSalad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoSalad.jpg"));
                    return;
                }
                else if (User.Weight - User.WeightGoal >= 50 && User.Weight - User.WeightGoal < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/berrysmoothie.jpg"));
                   _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                   _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/veggieWrap.jpg"));
                    return;

                }
                else if (User.Weight - User.WeightGoal < 50)
                {
                   _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/blueberryoaths.jpg"));
                   _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                   _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/veggieWrap.jpg"));

                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightLossOmnivoreFoods()
            {
                if (User.Weight - User.WeightGoal >= 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/greensmoothie.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/greekSalad.jpg"));
                     _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoSalad.jpg"));
                    return;
                }
                else if (User.Weight - User.WeightGoal >= 50 && User.Weight - User.WeightGoal < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/berrysmoothie.jpg"));
                   _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/tunaSalad.jpg"));
                   _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/chickenPotatoes.jpg"));
                    return;
                }
                else if (User.Weight - User.WeightGoal < 50)
                {
                   _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/blueberryoaths.jpg"));
                   _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/muffins.jpg"));
                   _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/potatoBeans.jpg"));
                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightGainVeganFoods()
            {
                if (User.WeightGoal - User.Weight >= 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/blueberryoaths.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg"));
                    return;
                }
                else if (User.WeightGoal - User.Weight >= 50 && User.WeightGoal - User.Weight < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/apple.png"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg"));                 
                    return;                   
                }
                else if (User.WeightGoal - User.Weight < 50)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/mango.png"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg"));

                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightGainVeggieFoods()
            {
                if (User.WeightGoal- User.Weight >= 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/pancakes.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/bagel.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/pizza.jpg"));
                    return;
                }
                else if (User.WeightGoal - User.Weight >= 50 && User.WeightGoal - User.Weight < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/omelette.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg"));
                    return;
                }
                else if (User.WeightGoal - User.Weight < 50)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/omelette.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/salad.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/stirFry.jpg"));

                }
            }
            /// <summary>
            /// checs user's weight difference
            /// </summary>
            /// <param name="weight"></param>
            /// <param name="weightGoal"></param>
            void WeightGainOmnivoreFoods()
            {


                if (User.WeightGoal - User.Weight >= 100)
                {


                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/biscuit.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/burger.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/chickenRice.jpg"));
                    return;
                }
                else if (User.WeightGoal - User.Weight >= 50 && User.WeightGoal - User.Weight < 100)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/sandwich.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/burrito.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/tunaPasta.jpg"));
                    return;

                }
                else if (User.WeightGoal - User.Weight < 50)
                {
                    _breakfastImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Breakfast/bacon.jpg"));
                    _lunchImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Lunch/bagel.jpg"));
                    _dinnerImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Dinner/pizza.jpg"));

                }
            }



        }
        //event handle to display the snack list
        private void Snack_Click(object sender, RoutedEventArgs e)
        {        
                _snack1.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/apple.png"));
                _snack2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/banana.png"));
                _snack3.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/grapes.png"));
                _snack4.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/mango.png"));
                _snack5.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/orange.png"));
                _snack6.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/strawberry.png"));
                _snack7.Source = new BitmapImage(new Uri("ms-appx:///Assets/Fruits/watermelon.png"));                  
        }

        //Going to the exercise page
        private void Go_To_Exercise(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage2));
        }
        //going to the schedule page
        private void Go_To_Schedule(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage3));
        }
    }
}

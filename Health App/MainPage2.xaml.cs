﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Gaming.Input.ForceFeedback;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Health_App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage2 : Page
    {
        // ================================================= FOR Exercise UI ============================================================== 
        MediaPlayer _mediaPlayer;
        MessageDialog musicDialog = new MessageDialog("Please make sure that your volume level is not at 0");
        Random randomObject = new Random();
        Exercise exerciseObject = new Exercise();
        public MainPage2()
        {
            this.InitializeComponent();
            UiLaunchGif();
            _mediaPlayer = new MediaPlayer();   
        }

        private void GoToMainPage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
        /// <summary>
        /// Plays the audio for motivational speeches for the user to listen to while exercising
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickMotivationalSpeech(object sender, RoutedEventArgs e)
        {
            RandomButtonAudio();
            MotivationalSpeech();
        }
        /// <summary>
        /// Plays the music for the user to listen to while exercising.
        /// This will be random music so that when the user clicks on the music 
        /// button they do not end up with same order of music which would be 
        /// very annoying for them if do not like a music that the application
        /// provides them with.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ClickMusic(object sender, RoutedEventArgs e)
        {
            RandomButtonAudio(); // Calls the method
            musicDialog.Title = "Volume Check!"; // This should be taken out and done when the
            // UI launches, or only ONCE (first time when the user clicks on this button)
            await musicDialog.ShowAsync();
            RandomMusic();
        
        }
        /// <summary>
        /// Responsible for randomizing the music
        /// </summary>
        public async void RandomMusic()
        {
            // Problem with this is that UNIQUE random numbers need to be generated in a loop, so that
            // the music keeps playing without stop. 

            int randNum = randomObject.Next(1, 7); // 1 to 6. Will increase this in a while to have more music :)

            // Find where the music is stored AKA assets folder
            var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");

            // Need to make the code below more efficient
            var file = await folder.GetFileAsync("you're never gonna make it Neffex - Fight Back.mp3");
            var file2 = await folder.GetFileAsync("08 No More Sorrow.m4a");
            var file3 = await folder.GetFileAsync("in-the-end-linkin-park.mp3");
            var file4 = await folder.GetFileAsync("Runaway Baby-Bruno Mars.mp3");
            var file5 = await folder.GetFileAsync("bad guy.mp3");
            var file6 = await folder.GetFileAsync("catch and release.mp3");
            var file7 = await folder.GetFileAsync("workoutHourLong.mp3");
            var file8 = await folder.GetFileAsync("AggressiveWorkoutMusic.mp3");
            var file9 = await folder.GetFileAsync("workoutMusic.mp3");
            // Problem to solve: Make a loop so that each music is played without stopping after one ends.
            // Same for motivational speech method.
            // TODO: Use 1 file, rather than all the 9 files. Tried this a while ago, and it did not work for some reason.
            // Works while debgging, but not while "running"
            switch (randNum)
            {
                case 1:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file);
                    break;
                case 2:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file2);
                    break;
                case 3:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file3);
                    break;
                case 4:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file4);
                    break;
                case 5:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file5);
                    break;
                case 6:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file6);
                    break;
                case 7:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file7);
                    break;
                case 8:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file8);
                    break;
                default:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file9);
                    break;
            }
            _mediaPlayer.AutoPlay = false;
            _mediaPlayer.Play();
        }
        /// <summary>
        /// Responsible for randomizing the audio for when the user clicks on a button. Button Click Audio.
        /// </summary>
        public async void RandomButtonAudio()
        {
            int randNum = randomObject.Next(1, 4); // 1 to 10 random number generated
            var assetsFolder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            var file2 = await assetsFolder.GetFileAsync("buttonClick3.mp3");
            var file3 = await assetsFolder.GetFileAsync("buttonClick2.mp3");
            var file4 = await assetsFolder.GetFileAsync("buttonClick.mp3");
            switch (randNum)
            {
                case 1:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file2);
                    break;
                case 2:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file3);
                    break;
                default:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file4);
                    break;
            }
            _mediaPlayer.AutoPlay = false;
            _mediaPlayer.Play();
        }
        /// <summary>
        /// Responsible for randomzing the motivational speech's when the user wants to listen
        /// to it and they click the appropriate button
        /// </summary>
        public async void MotivationalSpeech()
        {
            int randNum = randomObject.Next(1, 5); // 1 to 10 random number generated
            var assetFolder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Assets");
            var file = await assetFolder.GetFileAsync("LIFE IS SHORT.mp3");
            var file2 = await assetFolder.GetFileAsync("SELF DISCIPLINE.mp3");
            var file3 = await assetFolder.GetFileAsync("CHANGE YOUR LIFE.mp3");
            var file4 = await assetFolder.GetFileAsync("DO IT FOR YOU.mp3");
            var file5 = await assetFolder.GetFileAsync("I MUST NOT QUIT.mp3");
            var file6 = await assetFolder.GetFileAsync("lose Weight.mp3");

            switch (randNum)
            {
                case 1:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file);
                    break;
                case 2:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file2);
                    break;
                case 3:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file3);
                    break;
                case 4:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file4);
                    break;
                case 5:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file5);
                    break;
                default:
                    _mediaPlayer.Source = MediaSource.CreateFromStorageFile(file6);
                    break;
            }
            _mediaPlayer.AutoPlay = false;
            _mediaPlayer.Play();
        }
        private void RecommendedExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.RecommendedExercise();
            int randNum = randomObject.Next(1,5); // 1 to 4

            benchPress.Source = exerciseObject.Exercises[randNum >= 1 && randNum <= 3 ? randNum - 1 : 3];
            if (randNum==1)
            {
                instructionsFifteenTimes.Visibility = Visibility.Visible;
                EventHandlerVisibility();
                
            }
            else if(randNum == 2)
            {
                instructionsThirtySeconds.Visibility = Visibility.Visible;
                EventHandlerVisibility();
            }
            else if(randNum == 3)
            {
                instructionsFortySeconds.Visibility = Visibility.Visible;
                EventHandlerVisibility();
            }
            else
            {
                instructionsHundredTimes.Visibility = Visibility.Visible;
                EventHandlerVisibility();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EasyExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();

            exerciseObject.EasyExercise(); // It's the method inside the Exercise.cs class
            int randomNumber = randomObject.Next(1, 5); // 1 to 5
            benchPress.Source = exerciseObject.Exercises[randomNumber >= 1 && randomNumber <= 3 ? randomNumber - 1 : 3];
            switch (randomNumber)
            {
                case 1:
                    instructionsTenSeconds.Visibility = Visibility.Visible;
                    break;
                case 2:
                    instructionsOneTime.Visibility = Visibility.Visible;
                    break;
                case 3:
                    instructionsThreeTimes.Visibility = Visibility.Visible;
                    break;
                default:
                    instructionsFiveTimes.Visibility = Visibility.Visible;
                    break;
            }
            EventHandlerVisibility();
        }
        /// <summary>
        /// Will provide neither easy nor difficult exercises to the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediumDifficultExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.MediumDifficultyExercise();
            int randomNumber = randomObject.Next(1, 3); // 1 to 2
            if (randomNumber ==1 )
            {
                instructionsFortyTimes.Visibility = Visibility.Visible;
                benchPress.Source = exerciseObject.Exercises[0];
                EventHandlerVisibility();
            }
            else
            {
                instructionsThirtyTimes.Visibility = Visibility.Visible;
                benchPress.Source = exerciseObject.Exercises[1];
                EventHandlerVisibility();
            }
            // TODO: Need to add at least 5-6 more gifs here
        }
        /// <summary>
        /// Will provide the user with default difficult exercise, for 90% of the users (at least).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DifficultExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.DifficultExercise();
            int randomNumber = randomObject.Next(1, 2); // 1 to 2
            if (randomNumber == 1)
            {
                instructionsFortyTimes.Visibility = Visibility.Visible;
                benchPress.Source = exerciseObject.Exercises[0];
                EventHandlerVisibility();
            }
            // TODO: Need to add more gifs here
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToughExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.ToughExercise();

            // Randomize the exercise for the user to perform AND make sure that each exercise appears only once.
            Random randomizeExercise = new Random();
            var randArray = new int[9];

            for (int i = 0; i < 9; i++)
            {
                CollapseInstructions();
                var newRand = randomizeExercise.Next(1, 10);
                
                while (randArray.Contains(newRand))
                {
                    newRand = randomizeExercise.Next(1, 10);
                    benchPress.Source = exerciseObject.Exercises[newRand >= 1 && newRand <= 8 ? newRand - 1 : 8];
                }

                randArray[i] = newRand;
                switch (randArray[i])
                {
                    case 1:
                        instructionsHundredTimes.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        instructionsFiftyTimes.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        instructionsTwentyTimes.Visibility = Visibility.Visible;
                        break;
                    case 4:
                        instructionsHundredTimes.Visibility = Visibility.Visible;
                        break;
                    case 5:
                        instructionsTenTimes.Visibility = Visibility.Visible;
                        break;
                    case 6:
                        instructionsSixtyTimes.Visibility = Visibility.Visible;
                        break;
                    case 7:
                        instructionsTwentyTimes.Visibility = Visibility.Visible;
                        break;
                    case 8:
                        instructionsOneMinute.Visibility = Visibility.Visible;
                        break;
                    default:
                        instructionsFifteenTimes.Visibility = Visibility.Visible;

                        break;
                }
                EventHandlerVisibility();
            }
            
        }
        /// <summary>
        /// This method basically ensures that no instruction is placed on top of another, by "collapsing" the instructions.
        /// By instructions I mean the textblock that appears below the "Workout Plan" text block. 
        /// </summary>
        public void CollapseInstructions()
        {
            instructionsTenSeconds.Visibility = Visibility.Collapsed;
            instructionsTwentySeconds.Visibility = Visibility.Collapsed;
            instructionsThirtySeconds.Visibility = Visibility.Collapsed;
            instructionsFortySeconds.Visibility = Visibility.Collapsed;
            instructionsFiftySeconds.Visibility = Visibility.Collapsed;
            instructionsOneMinute.Visibility = Visibility.Collapsed;

            instructionsOneTime.Visibility = Visibility.Collapsed;
            instructionsTwoTimes.Visibility = Visibility.Collapsed;
            instructionsThreeTimes.Visibility = Visibility.Collapsed;

            instructionsFiveTimes.Visibility = Visibility.Collapsed;
            instructionsTenTimes.Visibility = Visibility.Collapsed;
            instructionsFifteenTimes.Visibility = Visibility.Collapsed;
            instructionsTwentyTimes.Visibility = Visibility.Collapsed;
            instructionsThirtyTimes.Visibility = Visibility.Collapsed;
            instructionsFortyTimes.Visibility = Visibility.Collapsed;
            instructionsFiftyTimes.Visibility = Visibility.Collapsed;
            instructionsSixtyTimes.Visibility = Visibility.Collapsed;
            instructionsHundredTimes.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// This method will only display the exercises that involve equipments. It will be called in its appropriate event
        /// handler method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EquipmentExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.EquipmentExercises();
            
            // 1 equals bench press
            int randNum = randomObject.Next(1,7); // 1 to 7
            benchPress.Source = exerciseObject.EquipmentExercise[randNum >= 1 && randNum <= 5 ? randNum - 1 : 5];
            if (randNum == 1)
            {
                instructionsThirtySeconds.Visibility = Visibility.Visible;
                // Did not have to set the source here because the bench press is available in the UI. just made it visible through the
                // method call.
            }
            // 2 equals manDumbell
            else if(randNum == 2)
            {
                instructionsTenTimes.Visibility = Visibility.Visible;
            }
            // 3 equals manDumbells
            else if(randNum == 3)
            {
                instructionsThirtySeconds.Visibility = Visibility.Visible;
            }
            // 4 equals manDumbellsGaining
            else if(randNum == 4)
            {
                instructionsTwentySeconds.Visibility = Visibility.Visible;
            }
            // 5 equals manGainingWeight
            else if(randNum == 5)
            {
                instructionsFiveTimes.Visibility = Visibility.Visible;
            }
            // 6 equals manGainingWeightDumb
            else
            {
                instructionsFifteenTimes.Visibility = Visibility.Visible;
            }
            EventHandlerVisibility();
        }
        /// <summary>
        /// The purpose of this method was to reduce repeating code so that code becomes more efficient.
        /// </summary>
        public void EventHandlerVisibility()
        {
            benchPress.Visibility = Visibility.Visible;
            schedule.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Responsible for providing the user with a "new" exercise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void NoEquipmentExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();
            exerciseObject.NoEquipmentExercise(); // Calls this method from the exercise class. That is why I created an object.

            int randomNumber = randomObject.Next(1,6); // 1 To 5
            benchPress.Source = exerciseObject.Exercises[randomNumber >= 1 && randomNumber <= 4 ? randomNumber - 1 : 4];
            // For the noEquipment4.gif
            if (randomNumber == 1)
            {
                instructionsFiveTimes.Visibility = Visibility.Visible;
            }
            // For the noEquipment5.gif
            else if (randomNumber == 2)
            {
                instructionsThirtySeconds.Visibility = Visibility.Visible;
            }
            // For the noEquipment6.gif
            else if (randomNumber == 3)
            {
                instructionsFifteenTimes.Visibility = Visibility.Visible;
            }
            // For the noEquipment7.gif
            else if (randomNumber == 4)
            {
                instructionsTenTimes.Visibility = Visibility.Visible;
            }
            // For the plank.gif
            else
            {
                instructionsOneMinute.Visibility = Visibility.Visible;
            }
            EventHandlerVisibility();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OldPeopleExercise(object sender, RoutedEventArgs e)
        {
            CollapseInstructions();

            exerciseObject.ExerciseForOldPeople();

            int randomNumber = randomObject.Next(1,6); // 1 to 5

            benchPress.Source = exerciseObject.OldPeopleExercise[randomNumber >= 1 && randomNumber <= 4 ? randomNumber - 1 : 4];
            if (randomNumber==1)
            {
                instructionsTenTimes.Visibility = Visibility.Visible;
            }
            else if(randomNumber==2)
            {
                instructionsThirtySeconds.Visibility = Visibility.Visible;
            }
            else if (randomNumber == 3)
            {
                instructionsFifteenTimes.Visibility = Visibility.Visible;
            }
            else if (randomNumber == 4)
            {
                instructionsTwentyTimes.Visibility = Visibility.Visible;
            }
            else 
            {
                instructionsTwentySeconds.Visibility = Visibility.Visible;
            }
            EventHandlerVisibility();
        }
        public void UiLaunchGif()
        {
            exerciseObject.StartUpScreenGif();
            int randNumb = randomObject.Next(1,4); // 1 to 3

            if (randNumb == 1)
            {
                benchPress.Source = exerciseObject.UiLaunchGifs[0];
                benchPress.Visibility = Visibility.Visible;
            }
            else if(randNumb == 2)
            {
                benchPress.Source = exerciseObject.UiLaunchGifs[1];
                benchPress.Visibility = Visibility.Visible;
            }
            else
            {
                benchPress.Source = exerciseObject.UiLaunchGifs[2];
                benchPress.Visibility = Visibility.Visible;
            }
        }
        private void NavigateToSchedule(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage3));
        }
    }
}
